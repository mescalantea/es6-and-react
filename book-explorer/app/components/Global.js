import React, { Component } from 'react';
import { Container, Form, FormGroup, FormControl, InputGroup, Button } from 'react-bootstrap';
import Gallery from './Gallery';

export default class Global extends Component {

    constructor(props) {
        super(props);
        this.state = {
            query: '',
            items: []
        };
    }

    search() {
        const baseUrl = 'https://www.googleapis.com/books/v1/volumes?q=';
        fetch(
            `${baseUrl}${this.state.query}`, {
            method: 'get'
        })
            .catch((e) => console.log(e))
            .then((response) => response.json())
            .then((json) => {
                let { items } = json;
                console.log(json);
                this.setState({ items });
            })
            ;
        // console.log(`search: ${this.state.query}`);
    }

    handleSubmit = (event) => {
        const form = event.currentTarget;
        event.preventDefault();
        this.search();
    }

    render() {
        return (
            <Container className="mt-5">
                <h1>Book Explorer!</h1>
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <InputGroup>
                            <FormControl
                                type="text"
                                placeholder="Search of a book"
                                required
                                onChange={event => this.setState({ query: event.target.value })}
                            />
                            <Button type="submit" variant="primary">Search</Button>

                        </InputGroup>
                    </FormGroup>
                </Form>
                <Gallery items={this.state.items} />
            </Container>
        );
    }
}