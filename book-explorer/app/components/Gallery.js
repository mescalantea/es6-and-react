import React, { Component } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';

export default class Gallery extends Component {
    render() {
        return (
            <Row>
                {
                    this.props.items.map((item, index) => {
                        let { title, imageLinks, infoLink } = item.volumeInfo;
                        return (
                            <Col key={index} sm={3} className='mb-4'>

                                <Card style={{ width: '100%' }}>
                                    <Card.Img style={{ height: '220px', objectFit: 'cover' }} variant="top" src={imageLinks !== undefined ? imageLinks.thumbnail : 'nocover.jpg'} />
                                    <Card.Body>
                                        <Card.Title  style={{ fontSize: '16px', fontWeight: '500', lineHeight: 'calc(1.4*16px)', minHeight: 'calc(3*1.4*16px)' }}>{title}</Card.Title>
                                        <Card.Text>
                                        </Card.Text>
                                        <Button variant="primary" onClick={(e) => {window.open(infoLink)}}>Read Book</Button>
                                    </Card.Body>
                                </Card>
                            </Col>
                        );
                    })
                }
            </Row>

        )
    }
}