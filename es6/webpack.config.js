const path = require('path');

module.exports = {
    // mode: 'development',
    mode: 'production',
    entry: [
        'regenerator-runtime/runtime.js',
        './app/index.js',
    ],
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },
    devServer: {
        contentBase: './build',
        port: 3000,
        inline: true
    }
}