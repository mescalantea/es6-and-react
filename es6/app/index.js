import { fellowship, total } from './fellowship';
import multiply, { add } from './math';
import Animal from './classes/Animal';
import Lion from './classes/Lion';

const limit = 100;
let arr1 = [20, 30, 40];
// spread operator example
let arr2 = [10, ...arr1, 50];
{
    let limit = 1;
}
let str = `Limit is: ${limit}`;
console.log(str);
console.log(arr1);
console.log(arr2);

// destructing assignment on arrays and object
let z = [4, 5, 6];
let [four, five] = z;
console.log(four, five);

let king = {
    name: "Simba",
    kids: 1
};

let { kids } = king;
console.log(kids);

// to assign already declared var using destructing assignment wrap inside ()
let name;
({ name } = king)
console.log(name);


// arrow functions
var cheer = () => {
    console.log("Woohoo!");
};

cheer();

// map helper method
let double = (n) => n * 2;
let doubled = arr1.map(double);

console.log(doubled);

// filter helper method
let filtered = arr1.filter((n) => n >= 40);
console.log(filtered);

// string helper methods
//repeat
let b = "wooh" + " ".repeat(50) + "oo";
console.log(b);

b = `wooh${"oo".repeat(15)}`;
console.log(b);

//startsWith
console.log("wooo".startsWith("woo"));

//endsWith
console.log("wooo!".endsWith("o!"));

//includes
console.log("wooo!".includes("oo"));

console.log(fellowship);
console.log(total);

console.log(multiply(2, 5));
console.log(add(2, 5));


// classes
const animal = new Animal("Mufasa", 4.5);
console.log(animal.hello());

const lion = new Lion("Simba", 3, "yellow");
console.log(lion.hello());
Lion.roar();

import { set, numbers, chars, map, numbersMap } from './datastruct'

console.log(set);
console.log(set.has(5));

console.log(numbers);
console.log(chars);

console.log(map);
// console.log(numbersMap);

for (let [key, value] of numbersMap.entries()) {
    console.log(`${key} => ${value}`);
}

// Private methods example

const obj = function () {
    // a private atribute
    let name;

    // public function
    const getName = () => {
        return name;
    };

    const setName = (n) => name = n;

    setName('My name');

    return { getName: getName };
};
let o = obj();
console.log(o);
console.log(o.getName());
// console.log(o.setName(''));
// console.log(o.name);


// using generators
function* countMaker() {
    let count = 0;
    while (true) {
        yield count += 1;
    }
}
let countGen = countMaker();
for (let index = 0; index < 100; index++) {
    console.log(countGen.next().value);
}


// Promises
let p = new Promise((resolve, reject) => {
    setTimeout(() => resolve('Resolve promise data'), 3000);
    // resolve('Resolve promise data');
    // reject('Rejected promise data');
});

p.then(
    response => console.log(response)
).catch(
    error => console.log(error)
);

// Fetch & chaining promises
const post_endpoint = "https://jsonplaceholder.typicode.com/posts/1";

fetch(post_endpoint, {
    method: "GET"
})
.catch(error => console.log(error))
.then(response => response.json())
.then(json => console.log(json));

const books_endpoint = "https://www.googleapis.com/books/v1/volumes?q=isbn:0747532699";

fetch(books_endpoint, {
    method: "GET"
})
.catch(error => console.log(error))
.then(response => response.json())
.then(json => console.log(json));


//-------------------------------
// ES6 / ES2015
//-------------------------------
// let, const
// Arrow functions () => {}
// Spread operator to join arrays
// Sets and Maps
// Import / Export
// Destructing assignment for objects and arrays
// Classes
// Generators function* / yield / next()
// Promises
// Fetch

//-------------------------------
// ES7 / ES2016
//-------------------------------
// Exponent operator
console.log(
    Math.pow(2, 5)
);
// now is the same as
console.log(
    2**5
);

// Includes for arrays
let arrInc = [1,2,3,4];
console.log(
    arrInc.includes(4)
);
console.log(
    arrInc.includes(7)
);

//-------------------------------
// ES8 / ES2017
//-------------------------------
// Object keys, values and entries functions
let objes8 = {
    a: 'one',
    b: 'two',
    c: 'three',
}

console.log(Object.keys(objes8));
console.log(Object.values(objes8));
console.log(Object.entries(objes8));

// Async Functions
async function async_one(){
    return "async one";
}
async_one().then(result => console.log(result));

async function async_two(){
    return "async one";
}

async function async_exc(){
    throw new Error('Issue with async!');
}
async_exc().catch(e => console.log(e));


async function async_three(){
    const one  = await async_one();
    console.log(one);
    const two = await async_two();
    console.log(two);
}

async_three();

async function async_four(){

    const [res_one, res_two] = await Promise.all(
        [async_one(), async_two()]
    );

    console.log(res_one, res_two);
}

async_four();