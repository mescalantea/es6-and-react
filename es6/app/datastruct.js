let set = new Set();

set.add(5);
set.add(43);
set.add("Woohoo");
set.add({
    x: 50,
    y: 100,
});

let numbers  = [1,2,3,1,2,3];
let numbersSet = new Set(numbers)

// how to filter unique values using sets from strings
let chars = "jdasdoqiwudoqdmjskldmjasasasdkjjasldjlasmxlasmx"
let charsSet = new Set(chars.split(""));


// Maps
let map = new Map();
map.set("key", "value");
map.set({a: "key"}, "value");
map.set(() => 'key', "value");

let numbersArr = [
    [1, 'one'],
    [2, 'two'],
    [3, 'three'],
];

let numbersMap = new Map(numbersArr);

export {
    set as set, 
    numbersSet as numbers, 
    charsSet as chars,
    map as map,
    numbersMap,
}