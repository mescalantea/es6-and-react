export default class Animal {
    constructor(name, height){
        this.name = name;
        this.height = height;
    }

    /**
     * Say hello
     * @returns String
     */
    hello(){
        return `Hi! I'm ${this.name} from animal kingdom`;
    }
}