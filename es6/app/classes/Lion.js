import Animal from "./Animal";
export default class Lion extends Animal {
    constructor(name, height, color) {
        super(name, height);
        this.color = color;
    }

    hello(){
        return `I'm ${this.name} from Pride Rock`;
    }

    static roar(){
        console.log("ROAR!!!");
    }
}